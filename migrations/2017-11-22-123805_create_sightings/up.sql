CREATE TABLE sightings (
    id INTEGER PRIMARY KEY NOT NULL,
    date_time TEXT NOT NULL,
    species TEXT NOT NULL,
    description TEXT NOT NULL,
    location TEXT,
    count INTEGER NOT NULL CHECK(count > 0)
);
