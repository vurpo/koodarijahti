# Vincit Koodarijahti -havaintopalvelin

Palvelin, joka täyttää Vincit [Koodarijahdin](http://koodarijahti.fi/) palvelinsovelluksen uudelleentoteutuksen vaatimukset. Vaatimusten lisäksi palvelin tukee myös paikkatietoa jos asiakassovellus tukee sitä.

Projekti on toteutettu [Rocket](https://rocket.rs)-sovelluskehyksellä ja SQLite-tietokannalla (käyttäen [Diesel](https://diesel.rs)-ORM-tietokantakirjastoa).

## Käyttö

Kehitysympäristössä vaaditaan [Rust-kehitystyökalut](https://www.rust-lang.org/en-US/install.html).

Kehitysympäristöön tarvitaan myös Diesel-tietokantatyökalut (`$ cargo install diesel_cli`) sekä SQLite-kehitysheaderit.

Koodi ei käänny ennen kuin tietokanta on luotu:

```plain
$ diesel setup
Creating database: database.db
Running migration 20171122123805
$ cargo build --release
```

Palvelin suoritetaan komennolla `cargo run`, tai suorittamalla valmis binääri: `target/release/koodarijahti`

Koodin testit voi suorittaa komennolla:

```plain
$ cargo test
```

## API

Palvelin toteuttaa seuraavat toiminnallisuudet:

### Tietomallit

#### `Sighting`

`id` (String): Havainnot yksilöivä merkkijono

`dateTime` (String): ISO 8601-formaatin mukainen aika (ilman aikavyöhykettä eli yleisaika) esim. 2016-12-12T10:10:00Z

`description` (String): Vapaamuotoinen tekstikenttä

`species` (String): Ankkalaji, jonka pitää vastata jotain /species-rajapinnan palauttamista arvoista.

`location` (String, optional): Havainnon sijainti. Paikkatieto on valinnainen, eli sovellus voi kirjata havaintoja ilman `location`-kenttää. Palvelin palauttaa myös sellaiset havainnot ilman tätä kenttää.

`count` (Number): Lukumäärä, paljonko ankkoja on näkynyt havainnossa. Pitää olla suurempi kuin nolla.

#### `Species`

`name` (String): Ankkalajin nimi

### Toiminnallisuudet

#### GET /sightings

Palauttaa kaikki kirjatut havainnot.

Tyyppi: `[Sighting]`

#### POST /sightings

Lisää uuden havainnon ja palauttaa uuden lisätyn havainnon.

Tyyppi: `Sighting`

#### GET /species

Palauttaa kaikki tuetut ankkalajit

Tyyppi: `[Species]`