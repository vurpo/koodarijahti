use super::schema::sightings;
use iso8601;

use errors::*;

/// A species of duck
#[derive(Serialize)]
pub struct Species<'a> {
    pub name: &'a str,
}

/// Sighting as retrieved from the database (id as integer)
#[derive(Queryable)]
pub struct DbSighting {
    pub id: i32,
    pub date_time: String,
    pub species: String,
    pub description: String,
    pub location: Option<String>,
    pub count: i32,
}

/// Sighting as returned through the HTTP REST API (id as string)
#[derive(Serialize)]
pub struct Sighting {
    pub id: String,
    #[serde(rename = "dateTime")] //Rust convention for names is snake_case, but API requires camelCase
    pub date_time: String,
    pub species: String,
    pub description: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub location: Option<String>,
    pub count: i32,
}

//type conversion from DbSighting into Sighting
impl From<DbSighting> for Sighting {
    fn from(sighting: DbSighting) -> Self {
        Sighting {
            id:             format!("{}", sighting.id), //format i32 into string
            date_time:      sighting.date_time,
            species:        sighting.species,
            description:    sighting.description,
            location:       sighting.location,
            count:          sighting.count,
        }
    }
}

/// New sighting to be inserted into database
#[derive(Insertable, Deserialize)]
#[table_name="sightings"]
pub struct NewSighting {
    #[serde(rename = "dateTime")]
    pub date_time: String,
    pub species: String,
    pub description: String,
    pub location: Option<String>,
    pub count: i32,
}


impl NewSighting {
    /// Validates that a NewSighting is semantically correct.
    pub fn validate(&self, valid_species: &[Species]) -> Result<()> {
        //Validate that the timestamp is valid ISO8601
        iso8601::datetime(&self.date_time)
            .map_err(|_| Error::from_kind(ErrorKind::InvalidTimestamp))?;

        //Validate the species
        if let None = valid_species.iter()
            .find(|i| i.name==self.species) {
            return Err(Error::from_kind(ErrorKind::InvalidSpecies));
        }

        //Validate that the count is > 0
        if self.count <= 0 {
            return Err(Error::from_kind(ErrorKind::InvalidCount));
        }

        Ok(())
    }
}
