use super::*;

/// Deserialize valid and invalid sighting JSON
mod json {
    use super::*;
    use models::*;

    fn test_sighting(sighting_json: &str) {
        serde_json::from_str::<NewSighting>(sighting_json).unwrap().validate(&SPECIES).unwrap();
    }

    /// Deserializes two valid JSON sightings
    #[test]
    fn deserialize_valid() {
        test_sighting("{\"dateTime\":\"2017-11-23T13:15:47.696Z\",\"species\":\"mallard\",\"description\":\"desc\",\"count\":1}");
        test_sighting("{\"dateTime\":\"2017-11-23T13:15:47.696Z\",\"species\":\"gadwall\",\"description\":\"desc\",\"count\":1, \"id\":\"123456\"}");
    }

    /// Attempts to deserialize two sightings with invalid count field
    #[test]
    #[should_panic]
    fn deserialize_invalid_count() {
        test_sighting("{\"dateTime\":\"2017-11-23T13:15:47.696Z\",\"species\":\"mallard\",\"description\":\"desc\",\"count\":0}");
        test_sighting("{\"dateTime\":\"2017-11-23T13:15:47.696Z\",\"species\":\"mallard\",\"description\":\"desc\",\"count\":-1}");
    }

    /// Attempts to deserialize two sightings with invalid species field
    #[test]
    #[should_panic]
    fn deserialize_invalid_species() {
        test_sighting("{\"dateTime\":\"2017-11-23T13:15:47.696Z\",\"species\":\"duck\",\"description\":\"desc\",\"count\":1}");
        test_sighting("{\"dateTime\":\"2017-11-23T13:15:47.696Z\",\"species\":\"goose\",\"description\":\"desc\",\"count\":1}");
    }

    /// Attempts to deserialize two sightings with invalid datetime field
    #[test]
    #[should_panic]
    fn deserialize_invalid_datetime() {
        test_sighting("{\"dateTime\":\"today\",\"species\":\"redhead\",\"description\":\"desc\",\"count\":1}");
        test_sighting("{\"dateTime\":\"December 21st\",\"species\":\"mallard\",\"description\":\"desc\",\"count\":1}");
    }

    /// Attempts to deserialize two sightings with missing fields
    #[test]
    #[should_panic]
    fn deserialize_missing_fields() {
        test_sighting("{\"species\":\"mallard\",\"description\":\"desc\",\"count\":1}");
        test_sighting("{\"dateTime\":\"2017-11-23T13:15:47.696Z\",\"description\":\"desc\",\"count\":1}");
        test_sighting("{\"dateTime\":\"2017-11-23T13:15:47.696Z\",\"species\":\"mallard\",\"count\":1}");
        test_sighting("{\"dateTime\":\"2017-11-23T13:15:47.696Z\",\"species\":\"mallard\",\"description\":\"desc\"}");
    }
}

/// Test correct database behavior
mod db {
    extern crate diesel_migrations;
    use models::*;
    use db::*;
    use diesel::prelude::*;
    /// Creates an in-memory database for testing only.
    /// Database is deleted when the connection is closed.
    fn open_test_database() -> SqliteConnection {
        // open SQLite in-memory database, not stored on disk
        let connection = SqliteConnection::establish(":memory:").unwrap();
        // create all the tables
        diesel_migrations::run_pending_migrations(&connection).unwrap();
        connection
    }

    /// Adds two new sightings to database, retreives all sightings, checks that there are two of them
    #[test]
    fn create_and_get_valid_sightings() {
        let conn = open_test_database();
        // NOTE: these new sightings are not validated, that's for the other tests to test
        let new_sighting1 = NewSighting {
            date_time: "2017-11-23T13:15:47.696Z".to_owned(),
            species: "mallard".to_owned(),
            description: "desc".to_owned(),
            location: None,
            count: 1
        };
        
        let new_sighting2 = NewSighting {
            date_time: "2017-11-23T13:15:47.696Z".to_owned(),
            species: "mallard".to_owned(),
            description: "desc".to_owned(),
            location: Some("here".to_owned()),
            count: 1
        };
        create_sighting(&conn, new_sighting1).unwrap();
        create_sighting(&conn, new_sighting2).unwrap();

        let sightings = get_all_sightings(&conn).unwrap();
        assert_eq!(sightings.len(), 2);
    } 

    /// Attempts to add sighting with invalid count column
    #[test]
    #[should_panic]
    fn create_invalid_sighting() {
        let conn = open_test_database();

        let new_sighting = NewSighting {
            date_time: "2017-11-23T13:15:47.696Z".to_owned(),
            species: "mallard".to_owned(),
            description: "desc".to_owned(),
            location: None,
            count: -1
        };
        create_sighting(&conn, new_sighting).unwrap();
    }
}
