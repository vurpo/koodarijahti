#![feature(plugin)]
#![plugin(rocket_codegen)]

// `error_chain!` can recurse deeply
#![recursion_limit = "1024"]

// error handling library
#[macro_use] extern crate error_chain;

// database library
#[macro_use] extern crate diesel;
#[macro_use] extern crate diesel_infer_schema;
extern crate dotenv;
extern crate r2d2_diesel;
extern crate r2d2;

// JSON serialiser/deserialiser
#[macro_use] extern crate serde_derive;
extern crate serde_json;

// HTTP server framework
extern crate rocket;
extern crate rocket_contrib;

// ISO8601 datetime parsing
// (could maybe be replaced with `regex` for more efficiency,
// since not all features of this crate are needed)
extern crate iso8601;

// contains the database schemas
pub mod schema;
// data models (for database and REST API)
pub mod models;
// functions for interacting with database
pub mod db;
// tests
#[cfg(test)] mod tests;

use rocket::fairing::{
    Fairing,
    Info,
    Kind
};
use rocket::{
    Request,
    Response
};

use rocket_contrib::Json;

// type definitions for error handling
mod errors {
    use rocket;
    error_chain!{
        errors { 
            InvalidTimestamp {
                description("timestamp not in valid ISO8601 format")
            }
            InvalidSpecies {
                description("not a valid species")
            }
            InvalidCount {
                description("invalid count")
                display("count must be > 0")
            }
            RocketError(error: rocket::error::LaunchError) {
                description("Rocket web server failed to run")
                display("{}", error)
            }
        }
    }

    use rocket::response::Responder;
    use rocket::Request;
    use rocket::Response;
    impl<'a> Responder<'a> for Error {
        fn respond_to(self, _: &Request) -> ::std::result::Result<Response<'a>,rocket::http::Status> {
            Err(rocket::http::Status::BadRequest)
        }
    }
}

pub use errors::*;

use models::Species;

///A const array of species, since there is never a need to modify the list of species.
///If the list of species was dynamic, this could be a database table instead. The Rust data models
///and architechture would remain mostly the same. However, since it does not need to be dynamic,
///defining it statically is much better for performance.
const SPECIES: [models::Species; 5] = [
    Species { name: "mallard" },
    Species { name: "redhead" },
    Species { name: "gadwall" },
    Species { name: "canvasback" },
    Species { name: "lesser scaup" },
];

#[get("/sightings")]
fn get_sightings(db_conn: db::DbConn) -> Result<Json<Vec<models::Sighting>>> {
    let sightings = db::get_all_sightings(&*db_conn)
        .chain_err(|| "unable to get sightings from database")?;
    
    Ok(Json(sightings))
}

#[post("/sightings", data="<sighting>")]
fn post_sightings(db_conn: db::DbConn, sighting: Json<models::NewSighting>) -> Result<Json<models::Sighting>> {
    let new_sighting = sighting.into_inner();

    new_sighting.validate(&SPECIES)
        .chain_err(|| "validate sighting failed")?;

    let sighting = db::create_sighting(&*db_conn, new_sighting)
        .chain_err(|| "insert sighting failed")?;

    Ok(Json(sighting))
}

#[get("/species")]
fn get_species() -> Result<Json<&'static [models::Species<'static>]>> {
    Ok(Json(&SPECIES))
}

/// Fairing (middleware) that adds the CORS header to every request
struct CorsFairing;
impl Fairing for CorsFairing {
    fn info(&self) -> Info {
        Info {
            name: "CORS fairing",
            kind: Kind::Response
        }
    }
    fn on_response(&self, _request: &Request, response: &mut Response) {
        response.set_header(rocket::http::Header::new("Access-Control-Allow-Origin", "*"));
    }
}

fn run() -> Result<()> {
    Err(Error::from_kind(ErrorKind::RocketError(
        rocket::ignite()
            .mount("/", routes![get_sightings, post_sightings, get_species])
            .manage(db::init_pool()?)
            .attach(CorsFairing)
            .launch())))
}

// error_chain -defined main() function
quick_main!(run);
