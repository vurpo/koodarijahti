use diesel;
use diesel::prelude::*;
use dotenv::dotenv;
use r2d2;
use r2d2_diesel::ConnectionManager;
use std::env;

use models;
use errors::*;

// Based on Rocket's r2d2+diesel example code

use std::ops::Deref;
use rocket::http::Status;
use rocket::request::{self, FromRequest};
use rocket::{Request, State, Outcome};

type Pool = r2d2::Pool<ConnectionManager<SqliteConnection>>;

pub struct DbConn(pub r2d2::PooledConnection<ConnectionManager<SqliteConnection>>);

/// Attempts to retrieve a single connection from the managed database pool. If
/// no pool is currently managed, fails with an `InternalServerError` status. If
/// no connections are available, fails with a `ServiceUnavailable` status.
impl<'a, 'r> FromRequest<'a, 'r> for DbConn {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<DbConn, ()> {
        let pool = request.guard::<State<Pool>>()?;
        match pool.get() {
            Ok(conn) => Outcome::Success(DbConn(conn)),
            Err(_) => Outcome::Failure((Status::ServiceUnavailable, ()))
        }
    }
}

// For the convenience of using an &DbConn as an &SqliteConnection.
impl Deref for DbConn {
    type Target = diesel::sqlite::SqliteConnection;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// Initializes a database pool.
pub fn init_pool() -> Result<Pool> {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL")
        .chain_err(|| "DATABASE_URL is not set")?;
    let manager = ConnectionManager::<SqliteConnection>::new(database_url);
    r2d2::Pool::builder()
        .max_size(1) // pool size is just 1, because an SQLite database can not be accessed concurrently
        .build(manager)
        .chain_err(|| "could not create database connection pool")
}

/// Adds a new sighting to the database and returns the added row.
pub fn create_sighting(conn: &SqliteConnection, new_sighting: models::NewSighting) -> Result<models::Sighting> {
    use schema::sightings::dsl::*;

    diesel::insert_into(sightings)
        .values(&new_sighting)
        .execute(conn)
        .chain_err(|| "failed to insert sighting into database")?;
    
    sightings
        .order(id.desc())
        .first::<models::DbSighting>(conn)
        .map(|s| s.into())
        .chain_err(|| "failed to get the inserted row from the database")
}

/// Returns all sightings stored in the database.
pub fn get_all_sightings(conn: &SqliteConnection) -> Result<Vec<models::Sighting>> {
    use schema::sightings::dsl::*;

    sightings.load::<models::DbSighting>(conn).map(|s| {
        s.into_iter().map(|i| i.into()).collect()
    })
        .chain_err(|| "failed to get sightings from database")
}
